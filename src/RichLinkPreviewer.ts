

var request: any = require('superagent/lib/client');

export class RichLinkPreviewer {

  constructor() {}

  private static validateURL(textval): boolean {
      var urlregex =
        /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
      return urlregex.test(textval);
    }

  public static getLinkMetaData(url, successCb, errorCb) {

    try {

      if (!url){
         return errorCb(new Error("missing url"));
      }

      if (!this.validateURL(url)){
        return errorCb(new Error("invalid url"));
      }

      request.post('https://graph.facebook.com')
        .type('form')
        .send({
          id: url
        })
        .send({
          scrape: true
        })
        .end(function (err, res) {
         
          if (err != null) {
            errorCb(err);
          } else {
            successCb(res.body);
          }
        });
    } catch (error) {

      errorCb(error);
    }

  }

}

