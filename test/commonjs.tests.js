var expect = require('chai').expect;
var cordovaLib = require('../dist/rich-link-previewer.min.js');

describe('RichLinkPreviewer', function () {
  it('is contained within rich-link-previewer as CommonJS', function () {
    expect(cordovaLib).to.be.an('object');
    expect(cordovaLib.RichLinkPreviewer).to.not.be.null;
  });

  it('can be instantiated', function () {
    var t = new cordovaLib.RichLinkPreviewer();
    expect(t).to.be.defined;
  });
});
