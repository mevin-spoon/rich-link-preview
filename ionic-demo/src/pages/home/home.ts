import {
  Component
} from '@angular/core';
import {
  NavController, LoadingController
} from 'ionic-angular';

import {
  RichLinkPreviewer
} from '../../../../dist/rich-link-previewer';
/*
import {
  RichLinkPreviewer
} from 'rich-link-previewer';

*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private url: string = "https://www.newscientist.com/article/mg23431213-000-offering-your-projects-a-helping-hand/";
  private hostName: string;
  private fetchInfo: string;
  private result: string;
  private showPreview: boolean = false;
  private title: string;
  private description: string;
  private imageUrl: string;

  constructor(public navCtrl: NavController, public loadingController: LoadingController) {

  }

  private validateURL(textval) {
    var urlregex =
      /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    return urlregex.test(textval);
  }

  private url_domain(data) {
    var a = document.createElement('a');
    a.href = data;
    return a.hostname;
  }

  private loadMetaData() {
    var self = this;
    if (!this.validateURL(self.url)) {
      alert('invalid url');
      return;
    };
    self.fetchInfo = "Fetching " + self.url;
    let loader = this.loadingController.create({
      content: "fetching metadata"
    });
    loader.present();

    var previewer = RichLinkPreviewer;
    previewer.getLinkMetaData(
      self.url,
      function (res) {
        loader.dismiss();
       
        if (!res.title) {
          alert('Metadata not found..please check url');
          return;
        }
        console.log('res=>', res);
        self.showPreview = true;
        self.title = res.title;
        self.description = res.description;
        self.hostName = self.url_domain(res.url);

        if (res.image.length > 0) {
          self.imageUrl = res.image[0].url;
        }
        self.result = "Raw response: \n" + JSON.stringify(res, null, 2);
      },
      function (error) {
        loader.dismiss();
        console.error(error);
        self.result = 'Error: ' + error.toString();
      });
  }

}
