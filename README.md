
## Rich Link Previewer 

This is a package used for extract appropriate title, description, content and images from a url,just like what you see when you post a link on facebook. This is possible using the [Open Graph Protocol](http://ogp.me/). Internally it uses [Facebook Open graph api](http://graph.facebook.com) to extract the metadata.

More info on Facebook crawler is available here:

[https://developers.facebook.com/docs/sharing/webmasters/crawler](https://developers.facebook.com/docs/sharing/webmasters/crawler)

[Sample screenshot](https://bytebucket.org/mevin-spoon/rich-link-preview/raw/665502863af9a9b367c742fd506a5a0a13c6b1b4/test/brower_test.png)


**Installation**

```
npm install --save https://mevin-spoon@bitbucket.org/mevin-spoon/rich-link-preview.git
```
**Sample usage**

On web, include the js library in your html:

```html
    <script src="rich-link-previewer.js"></script>
```

Then get a reference to the previewer:

```javascript
    var previewer = RichLinkLibrary.RichLinkPreviewer;
```

If using Typescript:
```javascript
    import { RichLinkPreviewer } from 'rich-link-preview';
    var previewer = RichLinkPreviewer;
```

To retrieve the metadata:
```javascript
    previewer.getLinkMetaData(
      url,
      function (res) {
        console.log( "Raw response: \n" + JSON.stringify(res, null, 2));

         if (!res.title) {
          console.log('Metadata not found..Check url');
          return;
        }

        console.log( "title: "+res.title);
        console.log( "description: "+res.description);
        console.log( "title: "+res.title);

        //images can be access using res.image which is an array of images
       
        
      },
      function (error) {
      });


```
**Sample json response**
```json
 {
    "url": "https://www.newscientist.com/article/mg23431213-000-offering-your-projects-a-helping-hand/",
    "type": "article",
    "title": "Unlocking the online potential of disadvantaged young people",
    "locale": {
        "locale": "en_us"
    },
    "image": [{
        "url": "https://d1o50x50snmhul.cloudfront.net/wp-content/uploads/2017/04/11180000/princes_trust_39.jpg"
    }],
    "description": "The Prince’s Trust is looking for volunteer e-mentors to help young people find their feet on the web and beyond",
    "site_name": "New Scientist",
    "updated_time": "2017-04-20T05:55:16+0000",
    "id": "1683664048356299"
}
```
If you are not getting the expected metadata, you can use Facebook's debugger to inspect the response:
[Facebook Open Graph Debugger](https://developers.facebook.com/tools/debug/)

**Demo**

This repository contains 2 versions of sample code:

[Browser Sample](./test/index.html)

[Ionic 2 Sample](./ionic-demo/src/)


## Credits
rich-link-preview is brought to you by [Spoon Consulting](http://www.spoonconsulting.com/).